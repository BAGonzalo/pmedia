# PMedia

The [repository](https://gitlab.com/BAGonzalo/pmedia) contains my best guess for the assigment described in the ```pmedia_assigment.doc``` file.

## Getting Started

The approach taken, results and comments are presented in ```.ipynb``` files for all sections; the first one is also included in the ```.doc``` file.

From Section 3, first (3.1) and last (3.3) proposed problems have been completed.

### Prerequisites and environment setting

To run the notebooks, use the image dockerfile provided [CPU-only support] as described in prerequisites.

[1] Install [Docker Engine](https://www.docker.com/community-edition#/download); and clone the repository.

[2] Build the image, and run the environment by:

```
cd pmedia
docker build -f dockerfile_cpu -t pmedia .
docker run -it -v {global-path-to-repo}/pmedia:/pmedia -p 9000:9000 --user root --shm-size 16G pmedia
cd pmedia
jupyter notebook --ip 0.0.0.0 --no-browser --port 9000 --allow-root 

```

[3] Access the Jupyter notebook tree at http://localhost:9000/, and enter the token provided by the console -e.g.:

```
[I 13:30:53.425 NotebookApp] The Jupyter Notebook is running at:
[I 13:30:53.426 NotebookApp] http://809b15d36bae:9000/?token=5b849c9e10fc2671579c113e270f3953ad33113120ce28d6
```

The pmedia folder will be visualised as a files tree in your browser.

[4] Open each individual notebook, and run it if desired.

## Built With

* [PyTorch](https://pytorch.org)

## Authors

* **BAGonzalo** - https://gitlab.com/BAGonzalo (bagonzalo@gmail.com)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* MF implementation, sightly based on torchmf: https://github.com/EthanRosenthal/torchmf

## To Explore

- DL & Tree-based methods with dynamic complexity (Taobao): https://arxiv.org/pdf/1801.02294.pdf

- Memory efficient C++ KNN implementation (Spotify): 
	https://github.com/spotify/annoy; (bpost) https://ebaytech.berlin/deep-learning-for-recommender-systems-48c786a20e1a

- Other TF wraper (2017): https://github.com/Leavingseason/OpenLearning4DeepRecsys

- Session based RS: https://arxiv.org/pdf/1711.04725.pdf

- Paper archive: 
	https://github.com/daicoolb/RecommenderSystem-Paper
	https://github.com/xanhxanh94/deep-learning-for-recommender-systems

## Feel free to discuss

If you end up going through this repository I encourage you to ask, complain or comment in the issues sections or by mail (bagonzalo@gmail.com) if there is any doubtful code implementation, comment or laid-out conclusion.