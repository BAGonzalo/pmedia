FROM ubuntu:16.04

RUN apt-get update && apt-get install -y --no-install-recommends apt-utils
RUN apt-get update && apt-get install -y rsync htop git openssh-server python-pip

# ## ----for running eos repository ---- ##
RUN apt-get update \
# 	&& apt-get install -y software-properties-common python-software-properties \
# 	&& add-apt-repository ppa:jonathonf/gcc-7.1 \
# 	&& apt-get update && apt-get install -y gcc-7 g++-7 \
# 	&& apt install -y libsm6 libxext6 \
# 	&& apt-get install -y libxrender-dev \
	&& apt-get install -y libgtk2.0-dev

## ———install miniconda——- ##
ENV PATH="/root/miniconda3/bin:${PATH}"

RUN apt-get update \
            && apt-get clean \
            && apt-get autoremove \
            && rm -rf /var/lib/apt/lists/* \
            && cd /tmp \
            && mkdir -p /tmp/miniconda \
            && wget https://repo.continuum.io/miniconda/Miniconda3-latest-Linux-x86_64.sh -P /tmp/miniconda | wc -l > /number

RUN bash /tmp/miniconda/Miniconda3-latest-Linux-x86_64.sh -b
RUN rm -rf /tmp/miniconda
RUN conda update -n base conda
RUN conda clean -ya
RUN export PATH

# update conda
RUN conda install -c menpo opencv3 && conda clean -ya
RUN conda install -c menpo dlib && conda clean -ya

RUN pip install --upgrade pip
RUN pip install cmake

## ———setting python & jupyter——- ##
# https://stackoverflow.com/questions/32373632/stop-python3-creating-module-cache-in-system-directory
RUN export PYTHONDONTWRITEBYTECODE="nocache"
#RUN export PYTHONDONTWRITEBYTECODE=1
RUN conda install jupyter && conda clean -ya
RUN conda install ipykernel && conda clean -ya
RUN python -m ipykernel install --user

## ——-install pytorch & torchvision——- ##

# # Install some basic utilities
# RUN apt-get update && apt-get install -y \
#     curl \
#     ca-certificates \
#     sudo \
#     git \
#     bzip2 \
#     libx11-6 \
#  && rm -rf /var/lib/apt/lists/*

RUN conda install pytorch-cpu torchvision-cpu -c pytorch && conda clean -ya
RUN conda install scipy && conda clean -ya
RUN conda install matplotlib && conda clean -ya
RUN conda install bcolz && conda clean -ya
RUN conda install scikit-image && conda clean -ya
RUN conda install scikit-learn && conda clean -ya
RUN conda install av -c conda-forge && conda clean -ya
RUN conda install conda xlrd && conda clean -ya
RUN conda install -c maciejkula -c pytorch spotlight=0.1.5 && conda clean -ya

# Install HDF5 Python bindings
RUN conda install -y \
    h5py \
 && conda clean -ya
RUN pip install h5py-cache

RUN pip install visdom
RUN pip install dominate
RUN pip install scikit-surprise
RUN pip install tensorboardX
RUN pip install tqdm